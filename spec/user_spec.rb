require 'spec_helper'

describe ExpertUol::User, :vcr do
  describe '#deactivate' do
    it 'deactivates the user' do
      user = ExpertUol::User.find_by_email('blin@uol.cz')

      expect(user.closed).to eq false

      user.deactivate

      expect(user.reload.closed).to eq true
    end
  end

  describe '.deactivate' do
    it 'deactivates the user' do
      user = ExpertUol::User.find_by_email('blin@uol.cz')

      expect(user.closed).to eq false

      ExpertUol::User.deactivate(user.id)

      expect(user.reload.closed).to eq true
    end
  end

  describe '.all' do
    it 'returns all entities collection' do
      users = ExpertUol::User.all

      expect(users.count).to eq 332
    end
  end

  describe '.find' do
    it 'searches entity by id' do
      user = ExpertUol::User.find(2)

      expect(user.id).to eq 2
    end

    it 'searches entity by email' do
      user = ExpertUol::User.find(email: 'test@uol.cz')

      expect(user.id).to eq 356
    end

    it 'returns nil for unexisted entity' do
      user = ExpertUol::User.find(email: 'qwe')

      expect(user).to eq nil
    end
  end

  describe '.each' do
    it 'iterates entities' do
      users = ExpertUol::User.all

      count = 0
      users.each { |_user| count += 1 }

      expect(count).to eq 332
    end
  end

  describe '.map' do
    it 'iterates entities' do
      users = ExpertUol::User.all
      ids = users.map(&:id)

      expect(ids.count).to eq 332
    end
  end

  describe '.collect' do
    it 'collects entities' do
      users = ExpertUol::User.all.collect

      expect(users.count).to eq 332
    end
  end
end
