require 'spec_helper'
require 'expert_uol/cli'

def cli(options = {})
  described_class.new([], options)
end

describe ExpertUol::Commands::Accounts, :vcr do
  describe 'list' do
    it 'returns a list' do
      output = capture_stdout { cli(fields: [:code]).invoke(:list) }.split

      expect(output).to include 'uol'
      expect(output).to include 'uolas'
    end

    it 'filters active accounts' do
      output = capture_stdout { cli(fields: [:code]).invoke(:list, ['active']) }.split

      expect(output).to include 'uol'
      expect(output).not_to include 'uolas'
    end

    it 'filters closed accounts' do
      output = capture_stdout { cli(fields: [:code]).invoke(:list, ['closed']) }.split

      expect(output).not_to include 'uol'
      expect(output).to include 'uolas'
    end
  end
end

describe ExpertUol::Commands::Companies, :vcr do
  describe 'list' do
    it "returns a list" do
      output = capture_stdout { cli(fields: [:name]).invoke(:list) }.split

      expect(output).to include 'uol'
    end
  end
end
describe ExpertUol::Commands::Contracts, :vcr do
  describe 'list' do
    it "returns a list" do
      output = capture_stdout { cli(fields: [:id, :type, :state]).invoke(:list) }.split

      expect(output).to include '1615,AccountingContract,active'
      expect(output).to include '1616,PayrollContract,active'
      expect(output).to include '1625,AccountingContract,closed'
    end

    it "filters by state" do
      output = capture_stdout { cli(fields: [:id, :type, :state], state: 'active').invoke(:list) }.split

      expect(output).to include '1615,AccountingContract,active'
      expect(output).to include '1616,PayrollContract,active'
      expect(output).not_to include '1625,AccountingContract,closed'
    end

    it "filters by type" do
      output = capture_stdout { cli(fields: [:id, :type, :state], type: 'payroll').invoke(:list) }.split

      expect(output).not_to include '1615,AccountingContract,active'
      expect(output).to include '1616,PayrollContract,active'
      expect(output).not_to include '1625,AccountingContract,closed'
    end
  end
end

describe ExpertUol::Commands::People, :vcr do
  describe 'list' do
    it "returns a list" do
      output = capture_stdout { cli(fields: [:id, :last_name, :first_name]).invoke(:list) }.split

      expect(output).to include '1854,Juriš,Michal'
    end
  end
end
