# COVERAGE=true rspec
if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start 'rails'
end

require 'expert_uol'

ExpertUol::Configuration.configure_from_common_yamls

# Requires supporting files with custom matchers and macros, etc,
# in ./support/ and its subdirectories.
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }


RSpec.configure do |config|
  config.order = :random

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect # Disable `should`
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect # Disable `should_receive` and `stub`
  end

  # Captures the output for analysis later
  #
  # @example capture_stdout
  #
  #     output = capture { $stderr.puts "this is captured" }
  #
  # @yield The block to capture stdout/stderr for.
  # @return [String] The contents of $stdout
  def capture_stdout
    begin
      $stdout = StringIO.new
      yield
      result = $stdout.string
    ensure
      $stdout = STDOUT
    end

    result
  end
end
