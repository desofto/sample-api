require 'spec_helper'

describe ExpertUol::Account, :vcr do
  let(:task) { 'create_wiki' } # don't accomplished this task with #task_completed

  describe '#find_by_code' do
    it 'searches by full code' do
      account = ExpertUol::Account.find_by_code('uol')
      expect(account.code).to eq 'uol'
    end

    it 'fails searching by partial code' do
      account = ExpertUol::Account.find_by_code('uo')
      expect(account).to be_nil
    end
  end

  it '#with_task' do
    accounts = ExpertUol::Account.with_task(task)
    expect(accounts.count).to eq 1
    expect(accounts.first.code).to eq 'uol'
  end

  it '#each_with_task' do
    count = 0
    ExpertUol::Account.each_with_task(task) do |account|
      count += 1
      expect(account.code).to eq 'uol'
    end
    expect(count).to eq 1
  end

  describe '#task_completed' do
    let(:account) { ExpertUol::Account.find(44) } # fixed in expert/seed

    it 'tells api that task is completed' do
      task = 'create_termix_dir'
      expect(account.task_completed(task)).to be_truthy
      expect(account.reload.tasks).to_not include task
    end

    it 'raise error if task is not present' do
      expect do
        account.task_completed('unknown_task')
      end.to raise_error ExpertUol::Client::Connection::InvalidEntity
    end
  end

  describe '#company' do
    it 'returns model' do
      account = ExpertUol::Account.find_by_code('uol')

      expect(account.company.id).to eq 2
    end

    it 'has emails' do
      account = ExpertUol::Account.find_by_code('uol')
      account.company.reload

      expect(account.company.email_addresses.count).to eq 1
      email = account.company.email_addresses.first
      expect(email.id).to eq 4772
      expect(email.location).to eq 'Work'
      expect(email.address).to eq 'blin@uol.cz'
    end
  end
end
