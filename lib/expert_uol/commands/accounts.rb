module ExpertUol
  module Commands
    class Accounts < Thor
      attr_writer :output

      desc "list [status]", "Show accounts filtered by status 'closed', 'active', etc., --fields=id"

      option :fields, type: :array,
                      default: [:code],
                      desc: 'Fields to show'

      def list(status = nil)
        fields = options[:fields]
        ExpertUol::Account.all.paginated do |account|
          next unless status.blank? || account.status == status
          data = fields.map { |field| account.send(field) }
          output.puts data.join(',')
        end
      end

      private

      def output
        @output ||= $stdout
      end
    end
  end
end
