module ExpertUol
  module Commands
    class People < Thor
      attr_writer :output

      desc "list", "Show people. --fields=id last_name first_name ..."

      option :fields, type: :array,
                      default: [:id, :last_name, :first_name],
                      desc: 'Fields to show'

      def list
        fields = options[:fields]
        ExpertUol::Person.all.paginated do |person|
          data = fields.map { |field| person.send(field) }
          output.puts data.join(',')
        end
      end

      private

      def output
        @output ||= $stdout
      end
    end
  end
end
