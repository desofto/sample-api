module ExpertUol
  module Commands
    class Companies < Thor
      attr_writer :output

      desc "list", "Show companies. --fields=id name ..."

      option :fields, type: :array,
                      default: [:id, :name],
                      desc: 'Fields to show'

      def list
        puts "!#{options.inspect}!"
        fields = options[:fields]
        ExpertUol::Company.all.paginated do |company|
          data = fields.map { |field| company.send(field) }
          output.puts data.join(',')
        end
      end

      private

      def output
        @output ||= $stdout
      end
    end
  end
end
