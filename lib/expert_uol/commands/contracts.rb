module ExpertUol
  module Commands
    class Contracts < Thor
      attr_writer :output

      desc "list", "Show contracts. --fields=id type ... --state=active --type=payroll"

      option :fields, type: :array,
                      default: [:id, :type],
                      desc: 'Fields to show'
      option :type, type: :string,
                    desc: 'Filter by type'
      option :state, type: :string,
                     desc: 'Filter by state'

      def list
        fields = options[:fields]
        contracts.paginated do |contract|
          data = fields.map { |field| contract.send(field) }
          output.puts data.join(',')
        end
      end

      private

      def contracts
        if filters.any?
          ExpertUol::Contract.where(filters)
        else
          ExpertUol::Contract.all
        end
      end

      def filters
        type = options[:type]
        state = options[:state]

        filters = {}
        filters[:type] = "#{type.capitalize}Contract" if type.present?
        filters[:state] = [state] if state.present?

        filters
      end

      def output
        @output ||= $stdout
      end
    end
  end
end
