module ExpertUol
  class Account < Base
    field_accessor :id, :code,
      :company_id, :company,
      :main_contact_id, :main_contact,
      :tasks,
      :subject_type, :referred_by_id, :referral_comment, :status

    SUBJECT_TYPES_TO_DB_TEMPLATES =
      { municipality:  'default_obec',
        school:        'default_skola',
        bank:          'default_banka',
        company_sk:    'default_sk',
        company:       'default_cz' }.freeze

    def main_contact
      @main_contact ||= if self[:main_contact].present?
                          Person.new(self[:main_contact])
                        else
                          Person.find(main_contact_id)
                        end
    end

    def company
      @company ||= if self[:company].present?
                     Company.new(self[:company])
                   else
                     Company.find(company_id)
                   end
    end

    def self.find_by_code(code)
      where(code: code).first
    end

    def self.with_task(task)
      where(task: task)
    end

    def self.each_with_task(task)
      with_task(task).each do |account|
        yield(account)
      end
    end

    def task_completed(task)
      put(:task_completed, task: task)
    end
  end
end
