module ExpertUol
  class Company < Contact
    def people
      @people ||= if self[:people].present?
                    Client::Collection.new(Person, self[:people])
                  else
                    Person.where(contact_id: id)
                  end
    end

    def self.find_by_ico(ico)
      where(ico: ico)
    end

    def self.with_account(code)
      where(account: { code: code }).first
    end

    def people_with_tag(tag)
      people.select { |i| i.tag_list.find { |t| t == tag} }
    end
  end
end
