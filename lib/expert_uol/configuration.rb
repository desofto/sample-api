require 'expert_uol/ext/accessors'

module ExpertUol
  module Configuration
    extend ExpertUol::Ext::Accessors
    #
    # All hashes in configuration has string keys so
    # loading setup from yaml works out of box
    #

    mattr_accessor :site
    @@site = { url:      'localhost',
               user:     'user',
               password: 'password' }

    # Hash with email addresses
    mattr_accessor :emails
    @@emails = { 'example' => 'example@uol.cz' }

    # Hash with email template strings
    mattr_accessor :email_templates
    @@email_templates = { 'dekujeme' => "\n\nDekujeme a prejeme prijemny den.\n",
                          'signatura' => "\n--  \nUcetnictvi on-line, s.r.o.\n272047000 - info@uol.cz - http://www.uol.cz\n" }

    mattr_accessor :smtp
    @@smtp = { 'host' => 'localhost',
               'port' => 25 }

    # Hash of hashes with ssh configurations
    mattr_accessor :ssh
    @@ssh = {}

    # Hash with applications root paths for use in bin scripts
    mattr_accessor :app_roots
    @@app_roots = {}

    # Flag marking if some type setup was run
    mattr_accessor :configured
    @@configured = false

    mattr_accessor :dry_mode
    @@dry_mode = false

    mattr_accessor :verbose_mode
    @@verbose_mode = false

    mattr_accessor :wiki_path
    @@wiki_path = "/var/www/dokuwiki/data/pages/"

    # Flag marking if no mails should be send
    mattr_accessor :no_mail
    @@no_mail = false

    mattr_accessor :db_otrs
    @@db_otrs = {}

    def self.configured?
      @@configured
    end

    def self.configure
      yield self
      @@configured = true
    end

    def self.configure_from_yaml(path)
      return if path.empty? || !File.exist?(path)

      yaml = YAML.load_file(path)
      yaml.each_pair do |key, value|
        if respond_to?(key)
          send("#{key}=", value)
        end
      end

      @@configured = true
    end

    def self.configure_from_common_yamls
      configure_from_yaml('/etc/expert.yml')
      configure_from_yaml(ENV['HOME']+'/.expert.yml')
      configure_from_yaml(File.dirname(__FILE__) + '/../../conf/expert.yml')
    end

    def self.parse_cli_arguments
      OptionParser.new do |opts|
        opts.banner = "Usage: #{File.basename($PROGRAM_NAME)} [arguments]"

        opts.on("-v", "Run verbosely") do |v|
          self.verbose_mode = true
        end

        opts.on("-dry", "Dry run") do |d|
          self.dry_mode = true
        end

        opts.on("-no-mail", "Don't send email") do |m|
          self.mo_mail = true
        end
      end.parse!

      @@configured = true
    end
  end
end
