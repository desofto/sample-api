module ExpertUol
  class Contact < Base
    field_accessor :id, :email_addresses

    def email_addresses
      @email_addresses ||= if self[:email_addresses].nil?
                             EmailAddress.where(contact_id: id)
                           else
                             Client::Collection.new(EmailAddress, self[:email_addresses])
                           end
    end

    def self.find_all_by_tag(tag)
      where(tag: tag)
    end
  end
end
