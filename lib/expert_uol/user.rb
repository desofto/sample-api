module ExpertUol
  class User < Base
    field_accessor :id, :email, :password
    field_accessor :email, :closed, :localnet_only, :api_only, :groups

    def self.find_by_email(email)
      find(email: email)
    end

    def self.deactivate(id)
      ActiveSupport::Deprecation.warn('ExpertUol::User.deactivate(:id) is deprecated. '\
                                      'Use ExpertUol::User.find(:id).deactivate instead.')
      user = User.find(id)
      user.deactivate
    end

    def deactivate
      put(:deactivate)
    end
  end
end
