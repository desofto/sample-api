require 'thor'

module ExpertUol
  module Commands
    $LOAD_PATH.unshift(File.dirname(__FILE__))

    autoload :Accounts,     'commands/accounts'
    autoload :People,       'commands/people'
    autoload :Companies,    'commands/companies'
    autoload :Contracts,    'commands/contracts'
  end

  class CLI < Thor
    option :verbose, :type => :boolean
    desc "accounts",          "Show accounts"
    subcommand 'accounts',    Commands::Accounts
    desc "people",            "Show people"
    subcommand 'people',      Commands::People
    desc "companies",         "Show companies"
    subcommand 'companies',   Commands::Companies
    desc "contracts",         "Show contracts"
    subcommand 'contracts',   Commands::Contracts
  end
end
