require 'rubygems'
require 'yaml'
require 'optparse'
require 'active_support/all'

module ExpertUol

  $:.unshift(File.dirname(__FILE__))
  autoload :Base,             'expert_uol/base'
  autoload :Account,          'expert_uol/account'
  autoload :Address,          'expert_uol/address'
  autoload :Contact,          'expert_uol/contact'
  autoload :ContactAddress,   'expert_uol/contact_address'
  autoload :Contract,         'expert_uol/contract'
  autoload :EmailAddress,     'expert_uol/email_address'
  autoload :FantozziContract, 'expert_uol/fantozzi_contract'
  autoload :PayrollContract,  'expert_uol/payroll_contract'
  autoload :Company,          'expert_uol/company'
  autoload :Person,           'expert_uol/person'
  autoload :User,             'expert_uol/user'
  autoload :PhoneCall,        'expert_uol/phone_call'
  autoload :BonoboAccessPermission, 'expert_uol/bonobo_access_permission'
  autoload :VERSION, 'expert_uol/version'

  autoload :AccountActivation, 'expert_uol/account_activation'

  autoload :Configuration,     'expert_uol/configuration'

  autoload :CLI, 'expert_uol/cli'

  module Utils
    autoload :Logger,       'expert_uol/utils/logger'
    autoload :Mailer,       'expert_uol/utils/mailer'
    autoload :Shell,        'expert_uol/utils/shell'
    autoload :SSH,          'expert_uol/utils/ssh'
    autoload :SqlRdf,       'expert_uol/utils/sql_rdf'

    autoload :CollectionWithRootParser, 'expert_uol/utils/collection_with_root_parser'
  end

  module Parsers
    autoload :Contact,      'expert_uol/parsers/contact'
    autoload :Company,      'expert_uol/parsers/company'
  end
end
